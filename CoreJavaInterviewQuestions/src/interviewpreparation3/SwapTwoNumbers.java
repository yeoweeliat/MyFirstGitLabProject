package interviewpreparation3;



//How to Swap two numbers without using third variable in Java?

public class SwapTwoNumbers {

	public static void main(String[] args) {
	
//		int a = 10;
//		int b = 20;
//
//		System.out.println("before swapping, a: " + a +" b: " + b);
//
//		//swapping value of two numbers without using temp variable
//		a = a + b; //now a is 30 and b is 20
//		b = a - b; //now a is 30 but b is 10 (original value of a)
//		a = a - b; //now a is 20 and b is 10, numbers are swapped
//
//		System.out.println("after swapping, a: " + a +" b: " + b);

		
		int a = 6;
		int b = 3;

		System.out.println("before swapping, a: " + a +" b: " + b);

		//swapping value of two numbers without using temp variable using multiplication and division
		a = a * b;
		b = a / b;
		a = a / b;

		System.out.println("after swapping using multiplication and division, a: " + a +" b: " + b);
	}

}
