package interviewpreparation3;


//Write a Java program that prints the numbers from 1 to 50. 
//For multiples of 3, print "Fizz" instead of the number
//For multiples of 5, print "Buzz"
//For multiples of both 3&5, print "FizzBuzz"

public class FizzBuzzTest {

	public static void main(String[] args) {

		
		 for(int i=1; i <= 50; i++) {
			 
	            if(i % (3*5) == 0) 
	            	System.out.println("FizzBuzz");
	            else if(i % 5 == 0) 
	            	System.out.println("Buzz");
	            else if(i % 3 == 0) 
	            	System.out.println("Fizz");
	            else 
	            	System.out.println(i);
	            
	        } 

	}

}
