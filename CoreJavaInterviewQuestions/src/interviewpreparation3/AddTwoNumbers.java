package interviewpreparation3;

import java.math.BigInteger;

//  How to add two integers without using arithmetic operator?

public class AddTwoNumbers {

	public static int add(int a, int b) {
		
		BigInteger i = BigInteger.valueOf(a);
		BigInteger j = BigInteger.valueOf(b);
		BigInteger sum = i.add(j); 
		
		return sum.intValue();
		
		}
	
	public static int add2(int a, int b) {
		
		//int result = (-a-b)*(-1);
		int result = a - (-b);
		
		return result; 
	}
	
	public static void main(String[] args) {
		
		int number1 = 2;
		int number2 = 3;
		
		System.out.println(add(number1,number2));
		System.out.println(add2(number1,number2));

	}

}
