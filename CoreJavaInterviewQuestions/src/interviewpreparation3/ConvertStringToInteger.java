package interviewpreparation3;

public class ConvertStringToInteger {

	public static void main(String[] args) {
		
		
//String to int
		
		 //using Integer.parseInt
		 int i = Integer.parseInt("123");
		 System.out.println("i: " + i);
	
		 
		//How to convert numeric string = "000000081" into Integer value = 81
		 int i2 = Integer.parseInt("000000081");
		 System.out.println("i2: " + i2);
		 
		 
		 
//int to string
		 
		 //Int to String in Java using "+" operator
		 String price1 = "" + 123;
		 System.out.println("price1: " + price1);
		 
		 
		 //converting int to String using String.format method:
		 String price2 = String.format("%d", 124);
		 System.out.println("price2: " + price2);
		 
		 
		 //After execution of above line Integer 123 will be converted into String �123�.
		 String price3 = String.valueOf(125); //recommended way
		 System.out.println("price3: " + price3);
		 
	
		System.out.println(new StringBuilder(price3).append(" ").append(10).toString());
		 
		 
	}

}
