package interviewpreparation3;

// How to check if an integer number is a power of 2

//doesn't work properly

public class CheckIntegerPowerOfTwo 
{

	public void powerOfTwo(int number)
	{
		int num = number;
		int d;
		boolean flag = true;
		
		while(num>1)
		{
			d = num % 2;
			
			if(d%2 != 0)
			{
				flag = false;
				break;
			}
			
			num = num/2;

		}
		
		if(flag == true)
		{
			System.out.println("Number " + number + " is a power of 2");
		}
		else
		{
			System.out.println("Number " + number + " is not a power of 2");
		}

	}
	public static void main(String[] args) 
	{

		int number = 9;
		CheckIntegerPowerOfTwo ref = new CheckIntegerPowerOfTwo();
	    ref.powerOfTwo(number);

	}

}