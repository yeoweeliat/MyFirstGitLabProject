package interviewpreparation;


import java.util.Scanner;
import java.util.Stack;


class Test {
	
	void FibonnaciSeries(){ //display fibonnaci sequence
		
		int num1 = 0;
		int num2 = 1;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of series: ");
		int length = sc.nextInt();
		
		int temp[] = new int[length]; //store series

		
		temp[0] = num1;
		temp[1] = num2;
		length = length - 2;
		
		for(int a=0; length > 0 ;a++){
			
			temp[2+a] = num1 + num2;
			
			num1 = num2;
			num2 = temp[2+a];
			
			length--;
		}
			
		for(int b : temp){
			
			System.out.print(b + " ");
		}
			
		}
	
	
	
	void FactorialNumber(){ //get factorial result of a number
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number: ");
		int num = sc.nextInt();
		
		int result = 1;
		
		while(num != 1){
		    	
			result = result*num;
			num = num - 1;
		}
		
		System.out.println("The factorial of this number is: " + result);
		
	}
	
	
	void ArmstrongNumber(){
		
		
		    Scanner sc = new Scanner(System.in);

			System.out.println("Enter number : ");
			int num = sc.nextInt();
		
//			int numOfDigits = 0;
//			
//			if(num/10 == 0)
//				numOfDigits = 1;
//			else if(num/100 ==0)
//				numOfDigits = 2;
//			else if(num/1000 ==0)
//				numOfDigits = 3;
//			else if(num/10000 ==0)
//				numOfDigits = 4;
//			
//			
//			
//			
//			switch(numOfDigits){
//			
//			case 1:
//				
//				if(((num%10)^3) == num){
//					
//					System.out.println("This is an armstrong number");
//				}
//				
//				else{
//					System.out.println("This is not an armstrong number");
//				}
//				
//			
//		    break;
//
//
//			case 3: 
			
			int result1 = (int) Math.pow(num%10 , 3);
					
			int result2 = ((num/10)%10)^3;
			int result3 = ((num/100)%10)^3;
			
			System.out.println(result1 + result2 + result3);
			
			
			if(result1 == num){
				
				System.out.println("This is an armstrong number");
			}
			else{
				
				System.out.println("This is not an armstrong number");
			}
				
		
//			default: 
//				System.out.println("Error, enter a number");
//	}
			
	}
	
		
//		System.out.println("Enter 3 numbers that are divisible by square root of 3");
//		
//		Scanner sc = new Scanner(System.in);
//
//		System.out.println("Enter first number : ");
//		int num1 = sc.nextInt();
//		System.out.println("Enter second number : ");
//		int num2 = sc.nextInt();
//		System.out.println("Enter third number : ");
//		int num3 = sc.nextInt();
//		
//		int temp[] = new int[10]; //store series
//		
//		for(int a=0; a<10 ;a++){ //assign values
//			
//			temp[a] = a*a*a;
//			
//			if (num1 == temp[a])
//				num1 = a;
//			if (num2 == temp[a])
//				num2 = a;
//			if (num3 == temp[a])
//				num3 = a;
//		}
//		
//		System.out.println("The result is: " + num1 + num2+ num3);
		
	
	
	
	void PrimeNumber(){ //check whether entered number is a prime number
		
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number: ");
		int num = sc.nextInt();
		
		
		
		if (num%2!= 0 && num%3!=0)
			System.out.println("This number is a prime number");
		else
			System.out.println("This number is not a prime number");
		
	}
	
	
	void SwapVariables(){ //swap 2 variables without using a third variable
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter variable 1: ");
		Object v1 = sc.next();
		
		System.out.println("Enter variable 2: ");
		Object v2 = sc.next();
		
		
//		Object temp = v1; //third variable
//		v1 = v2;
//		v2 = temp;
		
		
		Stack<Object> st = new Stack<Object>();
		st.push(v1);
		st.push(v2);
		
		v1 = st.pop(); //FILO manner
		v2 = st.pop();
		
		
		System.out.println("variable 1: " + v1 + ", variable 2: " + v2);
		
	}
	

	
	void ReverseString(){
		
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String: ");
		String s1 = sc.next();
		
		
		String temp[] = s1.split("");
		String temp2[] = new String[temp.length];
		

        for(int a=0; a<temp.length; a++){
        	
        	temp2[a] = temp[temp.length-1-a];
        	
        }
        
        String result = "";
        
        for(String a: temp2){
        	
        result = result + "" + a;
        
        }
        
        System.out.println(result);	
	
	}
			
	
}
	
	



public class LogicalCoding {

	public static void main(String[] args) {
		
		Test ref = new Test();
		
		//ref.FibonnaciSeries();
		
		//ref.FactorialNumber();
		
		ref.ArmstrongNumber();
		
		//ref.PrimeNumber();
		
		//ref.SwapVariables();
		
		//ref.ReverseString();
		
		

	}

}
