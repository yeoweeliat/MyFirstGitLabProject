package interviewpreparation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InterviewQuestions {

	public static void main(String[] args) {
		
		
		//Identify a given positive number as even/odd without using the % or / operator
		
//		int number = 10;
//		
//		while(number >= 2) 
//			number = number - 2;	
//		
//		if(number == 0)
//			System.out.println("number is an even number");
//		else
//			System.out.println("number is an odd number");
		
		
		
		//Convert given string "11/12/2010" to a Date object. 
		
		String dateString = "11/12/2010";
	    Date d = null;
	    
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		 
			try {
				d = sdf.parse(dateString);
			}
			
			catch (ParseException e) {
				e.printStackTrace();
			}
			
	        System.out.println(d);
	        
	        System.out.println(new Date());
	        System.out.println(new Date("12/11/2010"));
		
	
	        //Write a program that proves that Strings are not immutable in Java.
		
//	        String before = "ABCDEFG"; 
//	        String after = before.replace("ABC", "3Z"); 
//	        System.out.println("before = " + before); 
//	        System.out.println("after = " + after);
	        
	        
	}
}
