package interviewpreparation4;

import java.util.Arrays;

/**
 * Java program to implement insertion sort in Java. In this example, we will
 * sort an integer array using insertion sort algorithm. This logic can be used
 * to sort array of String, or any other object which implements Comparable or
 * Comparator interface in Java.
 * 
 * @author Javin Paul
 */

public class InsertionSort {
	
  public static void main(String args[]) {
	  
    // unsorted integer array
    int[] unsorted = { 32, 23, 45, 87, 92, 31, 19 };
    System.out.println("integer array before sorting : " + Arrays.toString(unsorted));

    insertionSort(unsorted);

    System.out.println("integer array after sorting : " + Arrays.toString(unsorted));
  }

  /*
   * Sort given integer array using Insertion sort algorithm. only good for
   * small arrays.
   */

  public static void insertionSort(int[] unsorted) {
	  
    for (int i=1; i < unsorted.length; i++) {
    	
      int current = unsorted[i];
      int j = i;

      // create right place by moving elements
      while (j > 0 && unsorted[j - 1] > current) {

        unsorted[j] = unsorted[j - 1]; // move
        j--;
      }

      unsorted[j] = current;  // found the right place, insert now
    }
  }
}