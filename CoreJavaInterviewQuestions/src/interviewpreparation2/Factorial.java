package interviewpreparation2;

public class Factorial {


	//find factorial of a number using recursion
	public static int factorial(int number){       
		
		//base case
		if(number == 0){
			return 1;
		}
		return number*factorial(number-1); //is this tail-recursion?
	}

	
	//calculate factorial using while loop or iteration
	public static int factorial2(int number){
		
		int result = 1;
		
		while(number != 0){
			result = result*number;
			number--;
		}

		return result;
	}


	public static void main(String[] args) {

		//finding factorial of a number in Java using recursion - Example
		System.out.println("factorial of 5 using recursion in Java is: " + factorial(5)); 

		//finding factorial of a number in Java using Iteration - Example 
		System.out.println("factorial of 6 using iteration in Java is: " + factorial2(6)); 
	}

}



