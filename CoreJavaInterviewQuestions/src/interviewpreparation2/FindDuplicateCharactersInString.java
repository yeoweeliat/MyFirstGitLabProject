package interviewpreparation2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


//find duplicate characters in String and print each of them.

public class FindDuplicateCharactersInString{
	
    public static void printDuplicateCharacters(String word) {
    	
        char[] characters = word.toCharArray();

        // build HashMap with character and number of times they appear in String
        //Map<Character,Integer> map = new HashMap<Character,Integer>(); //random order
        Map<Character,Integer> map = new LinkedHashMap<Character,Integer>(); //fixed insertion order
        
        
        for (Character ch : characters) {
        	
            if (map.containsKey(ch)) {
                map.put(ch, map.get(ch) + 1);
            } else {
                map.put(ch, 1);
            }
        }

        // Iterate through HashMap to print all duplicate characters of String
        Set<Map.Entry<Character, Integer>> entrySet = map.entrySet();
        System.out.printf("List of duplicate characters in String '%s' %n", word);
        
        for (Map.Entry<Character, Integer> entry : entrySet) {
            if (entry.getValue() > 1) {
                System.out.printf("%s : %d %n", entry.getKey(), entry.getValue());
            }
        }
    }
    
    
    public static void main(String args[]) {
    	
        printDuplicateCharacters("Programming");
        printDuplicateCharacters("Combination");
        printDuplicateCharacters("Java");
    }

   

}

