package interviewpreparation2;

//Reverse String

//check if a given String is palindrome. 
//A Palindrome is a String which is equal to the reverse of itself e.g. "Bob"

public class StringPalindrome {
	
	public static String reverse(String source){
		
        if(source == null || source.isEmpty()){
            return source;
        }       
        
        String reverse = "";
        
        for(int i = source.length() -1; i>=0; i--){
            reverse = reverse + source.charAt(i);
        }
      
        return reverse;
    }
	
	
	public static void main(String[] args) {
		
		
		//quick way to reverse String in Java - Use StringBuffer
        String word = "HelloWorld";
        String reverse = new StringBuffer(word).reverse().toString();
        System.out.printf("original String : %s , reversed String %s  %n", word, reverse);
      
        
        //another quick way to reverse String in Java - use StringBuilder
        word = "WakeUp";
        reverse = new StringBuilder(word).reverse().toString();
        System.out.printf("original String : %s , reversed String %s %n", word, reverse);
      
        
        //one way to reverse String without using StringBuffer or StringBuilder is writing own utility method
        word = "Band";
        reverse = reverse(word);
        System.out.printf("original String : %s , reversed String %s %n", word, reverse);
        
        
        int number = 1234;
        word = number + "";
        //word = Integer.toString(number);
        reverse = reverse(word);
        //reverse = new StringBuilder(word).reverse().toString();
        int reversedNumber = Integer.parseInt(reverse);
        System.out.printf("original number : %d , reversed number %d %n", number, reversedNumber);
        
    }   
}


