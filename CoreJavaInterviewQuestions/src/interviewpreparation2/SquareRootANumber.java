package interviewpreparation2;

import java.util.Scanner;

public class SquareRootANumber {

	public static void main(String[] args) {
		
        Scanner sc = new Scanner(System.in);
      
        System.out.println("Enter number to find square root in Java : ");
      
        double number = sc.nextDouble();
      
  
        //getting square root of a number in Java
        double squareRootNumber = Math.sqrt(number);
      
        //printing number and its square root in Java
        System.out.printf("Square root of number: %f is : %f %n" , number, squareRootNumber);
    

	}

}
