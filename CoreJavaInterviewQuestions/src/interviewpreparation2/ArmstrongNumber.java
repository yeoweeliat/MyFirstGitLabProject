package interviewpreparation2;

import java.util.Scanner;

//A number is called an Armstrong number if it is equal to the sum of cube/pow4/pow5 of each digit. 
//eg 153 is an Armstrong number because 153 = 1+125+27 = 1^3+5^3+3^3. 
//You need to write a program to check if given number is Armstrong number or not.

//armstrong numbers: 153, 371, 8208, 9474, 54748

public class ArmstrongNumber {

	 private static boolean isArmStrong(int number) {
		 
	        int result = 0;
	        int original = number;
	        int original2 = number;
	        int countDigits = 0;
	        
	        //count number of digits in number
	        while(original2 != 0) { 
	        	original2 = original2 / 10;
	        	countDigits++;
	        }
	        
	        
	        while(number != 0){
	            int remainder = number%10;
	            result = result + (int)Math.pow(remainder, countDigits);
	            number = number / 10;
	        }
	        
	        //if number is Armstrong return true
	        if(original == result){
	            return true;
	        }
	      
	        return false;
	    } 
	 
	 
	public static void main(String[] args) {
		
		//input number to check if its Armstrong number
        System.out.println("Please enter a number to find out if it is an Armstrong number:");
        
        int number = new Scanner(System.in).nextInt();
      
        //printing result
        if(isArmStrong(number)){
            System.out.println("Number : " + number + " is an Armstrong number");
        }else{
            System.out.println("Number : " + number + " is not an Armstrong number");
        }

	}

}
