package interviewpreparation2;

import java.util.Scanner;

//find Greatest Common Divisor or GCD of two numbers using Euclid�s method.

//LCD=(n1*n2)/GCD, Lowest Common Denominator = LCM

public class GCDOfTwoNumbers {
	
	private static int findGCD(int number1, int number2) { //recursion method
        //base case
        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1%number2);
    }
	
	private static int findLCM(int number1, int number2) {
		
		int lcm=0;
		
		for(int i=1;i<=number2;i++){
			lcm=number1*i;
			if(lcm%number2==0){
				break;
			}
		}
		return lcm;
	}
	
	private static int findLCD(int number1, int number2, int GCD) {

		int lcd = (number1*number2)/GCD;

		return lcd;
	}
	
	
	public static void main(String[] args) {

		//Enter two number whose GCD needs to be calculated.      
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter first number to find GCD");
        int number1 = scanner.nextInt();
        System.out.println("Please enter second number to find GCD");
        int number2 = scanner.nextInt();
      
        
        System.out.println("LCM of two numbers " + number1 + " and " + number2 + " is :" + findLCM(number1,number2));
        
        System.out.println("GCD of two numbers " + number1 + " and " + number2 + " is :" + findGCD(number1,number2));
      
        System.out.println("LCD of two numbers " + number1 + " and " + number2 + " is :" + findLCD(number1,number2,findGCD(number1,number2)));
        
        
        
	}

}
