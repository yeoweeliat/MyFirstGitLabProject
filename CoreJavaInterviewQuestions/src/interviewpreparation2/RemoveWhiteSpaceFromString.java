package interviewpreparation2;

/**
 * Java program to remove while space in String. In this program we will
 * see techniques to remove white space not only from beginning and end by using
 * trim() method of String class but also remove white space between String in Java.
 * e.g.  " ABC DEF " will turn to "ABCDEF". replaceAll() accepts regular expression
 * and \s can be used to remove all white space from String including space between
 * words.
 *
 * @author http://java67.blogspot.com
 */
public class RemoveWhiteSpaceFromString {
  
	
    public static void main(String args[]) {
      
    	
        //removing white space from String from beginning and end in Java       
        String strWhiteSpace = "    This String contains White Space at beginning and end and middle    ";
        System.out.printf("String before removing White space: %s %n", strWhiteSpace);
        System.out.printf("length of String before removing white space: %d %n %n", strWhiteSpace.length());
      
        
        //trim() method can remove white space from beginning and end of String in Java
        String strWithoutWhiteSpace = strWhiteSpace.trim();
        System.out.printf("String after removing white space from beginning and end: %s %n", strWithoutWhiteSpace);
        System.out.printf("length of String after removing white space from beginning and end: %d %n %n", strWithoutWhiteSpace.length());

        
        //removing white space between String in Java
        String whitespace = "   ABC DEF GHI   ";
        System.out.println("String with white space between words: " + whitespace);
      
        
        // \s is regular expression for white space tab etc
        String withoutspace = whitespace.replaceAll("\\s", "");
        System.out.println("String after removing white space between words and everywhere: " + withoutspace);       
      
    }     
    
}