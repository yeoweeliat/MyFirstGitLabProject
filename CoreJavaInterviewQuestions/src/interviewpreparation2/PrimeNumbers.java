package interviewpreparation2;

import java.util.Scanner;


//find all prime numbers up to a given number
//Prime number is not divisible by any number other than 1 and itself, return true if number is prime
	
public class PrimeNumbers {

	public static boolean isPrime(int number){

		for(int i=2; i<number; i++){
			if(number%i == 0){
				return false;
			}
		}
		return true; 
	}
	
	
	public static void main(String[] args) {

		System.out.println("Enter number: ");
		int limit = new Scanner(System.in).nextInt();

		for(int i=2; i<=limit; i++){
			if(isPrime(i)){
				System.out.println(i); //print prime numbers only
			}
		}

	}

}
