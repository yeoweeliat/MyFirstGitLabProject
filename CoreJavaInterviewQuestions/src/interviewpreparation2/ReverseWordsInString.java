package interviewpreparation2;

/**
 * Java Program to reverse words in String. There are multiple way to solve this
 * problem. you can either use any collection class e.g. List and reverse the
 * List and later create reverse String by joining individual words.
 *
 */

public class ReverseWordsInString {

	public static void main(String args[]) {

		String word = "This String will be getting reversed";
		String reversedWord="";

		for(int i = word.length()-1; i>=0; i--){
			reversedWord = reversedWord + word.charAt(i);
		}

		System.out.println(" Reversed Word : " + reversedWord); 

	}
}