package interviewpreparation2;

import java.util.Arrays;

// Write a program to check if two given Strings are Anagram. 
// Your function should return true if two Strings are Anagram, false otherwise. 
//A string is said to be an anagram if it contains same characters and same length but in different order 
//e.g. army and Mary

public class StringAnagram {
	
	/*
     * One way to find if two Strings are anagram in Java. This method
     * assumes both arguments are not null and in lowercase.
     *
     * @return true, if both String are anagram
     */
	
    public static boolean isAnagram(String word, String anagram){   
    	
        if(word.length() != anagram.length()){
            return false;
        }
       
        char[] chars = word.toCharArray();
       
        for(char c : chars){
            int index = anagram.indexOf(c);
            if(index != -1){
                anagram = anagram.substring(0,index) + anagram.substring(index +1, anagram.length());
            }else{
                return false;
            }           
        }
       
        return anagram.isEmpty();
    }
   
    /*
     * Another way to check if two Strings are anagram or not in Java
     * This method assumes that both word and anagram are not null and lowercase
     * @return true, if both Strings are anagram.
     */
    
    public static boolean iAnagram(String word, String anagram){
    	
        char[] charFromWord = word.toCharArray();
        char[] charFromAnagram = anagram.toCharArray();       
        Arrays.sort(charFromWord);
        Arrays.sort(charFromAnagram);
       
        return Arrays.equals(charFromWord, charFromAnagram);
    }
   
    public static boolean issAnagram(String firstWord, String secondWord) {
    	
    	char[] word1 = firstWord.replaceAll("[\\s]", "").toCharArray();
    	char[] word2 = secondWord.replaceAll("[\\s]", "").toCharArray();
    	Arrays.sort(word1);
    	Arrays.sort(word2);
    	
    	return Arrays.equals(word1, word2);
    }
    
   
    public static boolean checkAnagram(String first, String second){
    	
        char[] characters = first.toCharArray();
        StringBuilder sbSecond = new StringBuilder(second);
       
        for(char ch : characters){
            int index = sbSecond.indexOf("" + ch);
            if(index != -1){
                sbSecond.deleteCharAt(index);
            }else{
                return false;
            }
        }
       
        return sbSecond.length() == 0 ? true : false;
    }
    
    

	public static void main(String[] args) {
	
		String s1 = "mary";
		String s2 = "army";
		
		System.out.println("check anagram: " + isAnagram(s1,s2) + iAnagram(s1,s2) + issAnagram(s1,s2) + checkAnagram(s1,s2));
	}

}
