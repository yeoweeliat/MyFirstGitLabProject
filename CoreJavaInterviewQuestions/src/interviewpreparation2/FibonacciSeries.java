package interviewpreparation2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


//print Fibonacci series e.g. 1 1 2 3 5 8 13 ... up to a given number.

public class FibonacciSeries {
	
	void printFibonacciSeries(int num){
	
	int n1 = 1;
	int n2 = 1;
	int temp = 0;
	
	List<Integer> list = new ArrayList<Integer>(); //use list as it accepts duplicate values
	
	
	if(num <= 0)
		System.out.println("you have entered an invalid number");
	
	else if(num == 1)
		list.add(n1);
	
	else if (num == 2) {
		
		list.add(n1);
		list.add(n2);
	}
	
	else if (num > 2) {
		
		list.add(n1);
		list.add(n2);
		
		for(int i=3; i<=num; i++) { //actual logic
			
			temp = n2;
			n2 = n1 + n2;
			n1 = temp;
			
			list.add(n2);
		}
	}	
	
	System.out.println("Fibonacci Series: " + list);
}
	
	
	public static void main(String[] args) {

		FibonacciSeries ref = new FibonacciSeries();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Print Fibonacci series until which number: ");
		int number = sc.nextInt();
		
		ref.printFibonacciSeries(number);
		
	}

}
